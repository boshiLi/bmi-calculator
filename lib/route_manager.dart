import 'package:bmi_calculator/pages/input_page.dart';
import 'package:bmi_calculator/pages/result_page.dart';
import 'package:flutter/material.dart';

enum RouteType { input, result }

class RouteManager {
  static String getRouteKey(RouteType type) {
    switch (type) {
      case RouteType.input:
        return '/';
      case RouteType.result:
        return '/result';
      default:
        return null;
    }
  }

  static void push(RouteType route, BuildContext context) {
    Navigator.pushNamed(context, RouteManager.getRouteKey(route));
  }
}

Map<String, WidgetBuilder> kRoutes = {
  RouteManager.getRouteKey(RouteType.input): (context) => InputPage(),
  RouteManager.getRouteKey(RouteType.result): (context) => ResultPage(),
};
