import 'dart:math';

class BMIBrain {
  final int height;
  final int weight;

  double _bmi() => weight / pow(height / 100, 2);
  BMIBrain({this.height, this.weight});

  String calculate() {
    return _bmi().toStringAsFixed(1);
  }

  String result() {
    BMIDataManager data = BMIDataManager(this._bmi());
    return data.bmiKey(data.result()).toUpperCase();
  }

  String suggest() {
    BMIDataManager data = BMIDataManager(this._bmi());
    return data.bodyAdvice();
  }
}

class BMIDataManager {
  final double bmi;
  BMIDataManager(this.bmi);

  Map<String, String> bmiBodyMap() {
    return {
      this.bmiKey(BMIBodyType.overweight): '哦！「體重過重」了，要小心囉，趕快力行「健康體重管理」！',
      this.bmiKey(BMIBodyType.normal): '恭喜！「健康體重」，要繼續保持！',
      this.bmiKey(BMIBodyType.underweight): '「體重過輕」，需要多運動，均衡飲食，以增加體能，維持健康！'
    };
  }

  BMIBodyType result() {
    if (this.bmi >= 25) {
      return BMIBodyType.overweight;
    } else if (bmi > 18) {
      return BMIBodyType.normal;
    } else {
      return BMIBodyType.underweight;
    }
  }

  String bmiKey(BMIBodyType type) {
    switch (type) {
      case BMIBodyType.overweight:
        return 'Over Weight';
      case BMIBodyType.normal:
        return 'Normal';
      case BMIBodyType.underweight:
        return 'Under Weight';
      default:
        return null;
    }
  }

  String bodyAdvice() => this.bmiBodyMap()[this.bmiKey(this.result())];
}

enum BMIBodyType { overweight, normal, underweight }
