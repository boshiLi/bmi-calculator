import 'package:bmi_calculator/bmi_brain.dart';
import 'package:bmi_calculator/pages/result_page.dart';
import 'package:bmi_calculator/route_manager.dart';
import 'package:flutter/material.dart';
import 'package:bmi_calculator/ReuseableWidgets/bottom_button.dart';
import 'package:bmi_calculator/ReuseableWidgets/reusable_card.dart';
import 'package:bmi_calculator/ReuseableWidgets/gender_content_widget.dart';
import '../consts.dart';
import '../ReuseableWidgets/height_slider_widget.dart';
import '../ReuseableWidgets/integer_selector_widget.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  GenderType selectedGender = GenderType.male;
  int height = 150;
  int weight = 50;
  int age = 22;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ReusableCard(
                    onPressed: () {
                      setState(() {
                        this.selectedGender = GenderType.male;
                      });
                    },
                    color: this.selectedGender == GenderType.male
                        ? kActiveCardColor
                        : kInActiveCardColor,
                    child: GenderContentWidget(
                      gender: Gender(
                        genderType: GenderType.male,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: ReusableCard(
                    onPressed: () {
                      setState(() {
                        this.selectedGender = GenderType.female;
                      });
                    },
                    color: this.selectedGender == GenderType.female
                        ? kActiveCardColor
                        : kInActiveCardColor,
                    child: GenderContentWidget(
                      gender: Gender(
                        genderType: GenderType.female,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ReusableCard(
              color: kActiveCardColor,
              child: HeightSliderWidget(
                defaultHeight: this.height,
                inActiveColor: Color(0xFF8D8E98),
                activeColor: Colors.white,
                thumbColor: Color(0xFFEB1555),
                overlayColor: Color(0x29EB1555),
                maxHeight: 230,
                minHeight: 120,
                onChanged: (newValue) {
                  this.height = newValue;
                },
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ReusableCard(
                    color: kActiveCardColor,
                    child: IntegerSelectorWidget(
                      'WEIGHT',
                      weight: this.weight,
                      onChanged: (newValue) {
                        setState(() {
                          this.weight = newValue;
                        });
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: ReusableCard(
                    color: kActiveCardColor,
                    child: IntegerSelectorWidget(
                      'AGE',
                      weight: this.age,
                      onChanged: (newValue) {
                        setState(() {
                          this.age = newValue;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          BottomButton(
            title: 'CALCULATE',
            onPressed: () {
              BMIBrain brain =
                  BMIBrain(height: this.height, weight: this.weight);
              MaterialPageRoute route = MaterialPageRoute(
                builder: (context) {
                  return ResultPage(
                    result: brain.result(),
                    bmi: brain.calculate(),
                    suggest: brain.suggest(),
                  );
                },
              );
              Navigator.push(context, route);
            },
          ),
        ],
      ),
    );
  }
}
