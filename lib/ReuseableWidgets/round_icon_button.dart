import 'package:flutter/material.dart';

class RoundIconButton extends StatelessWidget {
  final VoidCallback onPressed;
  final IconData iconData;
  final double size;
  final Color fillColor;
  const RoundIconButton({
    @required this.onPressed,
    this.iconData,
    this.size,
    this.fillColor,
  });

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Icon(
        this.iconData,
      ),
      shape: CircleBorder(),
      constraints: BoxConstraints.tightFor(
        width: this.size,
        height: this.size,
      ),
      elevation: 6.0,
      fillColor: this.fillColor,
      onPressed: this.onPressed,
    );
  }
}
