import 'package:flutter/material.dart';
import 'package:bmi_calculator/consts.dart';

class HeightSliderWidget extends StatefulWidget {
  final int minHeight;
  final int maxHeight;
  final int defaultHeight;
  final ValueChanged<int> onChanged;
  final Color inActiveColor;
  final Color activeColor;
  final Color thumbColor;
  final Color overlayColor;
  const HeightSliderWidget({
    @required this.defaultHeight,
    @required this.minHeight,
    @required this.maxHeight,
    @required this.onChanged,
    this.inActiveColor,
    this.activeColor,
    this.thumbColor,
    this.overlayColor,
  });

  @override
  _HeightSliderWidgetState createState() => _HeightSliderWidgetState(
        defaultHeight: defaultHeight,
        minHeight: minHeight,
        maxHeight: maxHeight,
        onChanged: onChanged,
        inActiveTrackColor: inActiveColor,
        activeTrackColor: activeColor,
        thumbColor: thumbColor,
        overlayColor: overlayColor,
      );
}

class _HeightSliderWidgetState extends State<HeightSliderWidget> {
  int defaultHeight;
  final int minHeight;
  final int maxHeight;
  final ValueChanged<int> onChanged;
  final Color inActiveTrackColor;
  final Color activeTrackColor;
  final Color thumbColor;
  final Color overlayColor;

  _HeightSliderWidgetState({
    this.defaultHeight,
    this.minHeight,
    this.maxHeight,
    this.onChanged,
    this.inActiveTrackColor,
    this.activeTrackColor,
    this.thumbColor,
    this.overlayColor,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'HEIGHT',
          style: kLabelTextStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.baseline,
          textBaseline: TextBaseline.alphabetic,
          children: <Widget>[
            Text(
              this.defaultHeight.toString(),
              style: kBoldLabelTextStyle,
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              'cm',
              style: kLabelTextStyle,
            )
          ],
        ),
        SliderTheme(
          data: SliderTheme.of(context).copyWith(
            activeTrackColor: this.activeTrackColor,
            inactiveTrackColor: this.inActiveTrackColor,
            thumbColor: this.thumbColor,
            overlayColor: this.overlayColor,
            thumbShape: RoundSliderThumbShape(
              enabledThumbRadius: 15,
            ),
            overlayShape: RoundSliderOverlayShape(
              overlayRadius: 30,
            ),
          ),
          child: Slider(
            max: this.maxHeight.toDouble(),
            min: this.minHeight.toDouble(),
            value: this.defaultHeight.toDouble(),
            onChanged: (newValue) {
              setState(() {
                this.defaultHeight = newValue.round();
                this.onChanged(newValue.round());
              });
            },
          ),
        )
      ],
    );
  }
}
