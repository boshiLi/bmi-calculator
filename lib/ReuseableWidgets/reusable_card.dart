import 'package:flutter/material.dart';
import 'package:bmi_calculator/consts.dart';

class ReusableCard extends StatelessWidget {
  final Color color;
  final Widget child;
  ReusableCard({@required this.color, this.child, this.onPressed});
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        child: this.child,
        margin: EdgeInsets.all(15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: this.color,
        ),
      ),
    );
  }
}
