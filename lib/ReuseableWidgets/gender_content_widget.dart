import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bmi_calculator/consts.dart';

class GenderContentWidget extends StatelessWidget {
  final Gender gender;
  GenderContentWidget({this.gender});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            this.gender.iconData,
            size: 80,
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            this.gender.describe,
            style: kLabelTextStyle,
          )
        ],
      ),
    );
  }
}

class Gender {
  String describe;
  IconData iconData;
  GenderType genderType;

  Gender({GenderType genderType}) {
    this.genderType = genderType;
    switch (genderType) {
      case GenderType.male:
        this.describe = 'MALE';
        this.iconData = FontAwesomeIcons.mars;
        break;
      case GenderType.female:
        this.describe = 'FEMALE';
        this.iconData = FontAwesomeIcons.venus;
        break;
    }
  }
}

enum GenderType {
  male,
  female,
}
