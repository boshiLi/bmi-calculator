import 'package:flutter/material.dart';
import 'package:bmi_calculator/consts.dart';

class BottomButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;
  BottomButton({this.onPressed, this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      width: double.infinity,
      height: 80.0,
      child: GestureDetector(
        child: Container(
          color: kBottomButtonColor,
          child: Container(
            padding: EdgeInsets.only(bottom: 10),
            child: Center(
              child: Text(
                this.title,
                style: kLargeButtonTextStyle,
              ),
            ),
          ),
        ),
        onTap: this.onPressed,
      ),
    );
  }
}
