import 'package:flutter/material.dart';
import 'round_icon_button.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bmi_calculator/consts.dart';

class IntegerSelectorWidget extends StatefulWidget {
  final String contentTitle;
  final int weight;
  final ValueChanged<int> onChanged;

  IntegerSelectorWidget(
    this.contentTitle, {
    @required this.weight,
    @required this.onChanged,
  });

  @override
  _IntegerSelectorWidgetState createState() => _IntegerSelectorWidgetState(
        contentTitle,
        weight: this.weight,
        onChanged: this.onChanged,
      );
}

class _IntegerSelectorWidgetState extends State<IntegerSelectorWidget> {
  _IntegerSelectorWidgetState(
    this.contentTitle, {
    this.weight,
    this.onChanged,
  });
  int weight;
  final String contentTitle;
  final ValueChanged<int> onChanged;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          this.contentTitle,
          style: kLabelTextStyle,
        ),
        Text(
          this.weight.toString(),
          style: kBoldLabelTextStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RoundIconButton(
              fillColor: kRoundedButtonColor,
              iconData: FontAwesomeIcons.plus,
              size: 56,
              onPressed: () {
                setState(() {
                  this.weight++;
                  onChanged(weight);
                });
              },
            ),
            SizedBox(
              width: 15,
            ),
            RoundIconButton(
              fillColor: kRoundedButtonColor,
              iconData: FontAwesomeIcons.minus,
              size: 56,
              onPressed: () {
                setState(() {
                  this.weight--;
                  onChanged(weight);
                });
              },
            )
          ],
        )
      ],
    );
  }
}
