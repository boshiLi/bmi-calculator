import 'package:flutter/material.dart';

const Color kBottomButtonColor = Color(0xFFEB1555);
const Color kActiveCardColor = Color(0xFF1D1E33);
const Color kInActiveCardColor = Color(0xFF111328);
const Color kRoundedButtonColor = Color(0xFF4C4F5E);
const TextStyle kLabelTextStyle = TextStyle(
  fontSize: 18,
  color: Color(0xFF8D8E98),
);

const TextStyle kBoldLabelTextStyle = TextStyle(
  fontSize: 50,
  letterSpacing: 1.6,
  fontWeight: FontWeight.w900,
);

const TextStyle kLargeButtonTextStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.bold,
);

const TextStyle kTitleTextStyle = TextStyle(
  fontSize: 50,
  fontWeight: FontWeight.bold,
);

const TextStyle kResultTextStyle = TextStyle(
  fontSize: 22,
  fontWeight: FontWeight.bold,
  color: Color(0xFF24D876),
);

const TextStyle kBMITextStyle = TextStyle(
  fontSize: 100,
  fontWeight: FontWeight.bold,
);

const TextStyle kBodyTextStyle = TextStyle(
  fontSize: 22,
);
